#!/usr/bin/env node

const http = require("http");

const options = {
  host: "localhost",
  port: "8000",
  timeout: 2000,
};

const request = http.request(options, (res) => {
  if ([200, 302].includes(res.statusCode)) {
    console.log(`OK STATUS: ${res.statusCode}`);
    process.exit(0);
  } else {
    console.log(`ERROR STATUS: ${res.statusCode}`);
    process.exit(1);
  }
});

request.on("error", () => {
  console.log("ERROR");
  process.exit(1);
});

request.end();
