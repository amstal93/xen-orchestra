#!/usr/bin/env sh

set -e

if [ "$1" = 'xo-server' ] && [ "$(id -u)" = '0' ]; then
  chown node: ./ -R
  exec su-exec node "$0" "$@"
fi

exec "$@"
