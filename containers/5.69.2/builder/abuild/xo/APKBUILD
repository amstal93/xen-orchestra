# Contributor: Artem Korezin <korezin.artem@sintek-nn.ru>
# Maintainer: Artem Korezin <korezin.artem@sintek-nn.ru>
pkgname=xen-orchestra
pkgdesc='The complete web solution for XenServer/XCP-ng'
pkgver=5.69.2
pkgrel=0
_commit=7c7ee7fb9b32c3ec462467ba2b95b9a47d74cd5c
arch='all'
url='https://github.com/vatesfr/xen-orchestra'
license='AGPL-3.0-or-later'

# renovate: datasource=repology depName=alpine_3_15/util-linux versioning=loose
depends="$depends util-linux=2.37.4-r0"
# renovate: datasource=repology depName=alpine_3_15/lvm2 versioning=loose
depends="$depends lvm2=2.02.187-r2"
# renovate: datasource=repology depName=alpine_3_15/libstdc++ versioning=loose
depends="$depends libstdc++=10.3.1_git20211027-r0"
# renovate: datasource=repology depName=alpine_3_15/openssl versioning=loose
depends="$depends openssl=1.1.1o-r0"

# renovate: datasource=repology depName=alpine_3_15/python3 versioning=loose
makedepends="$makedepends python3=3.9.7-r4"
# renovate: datasource=repology depName=alpine_3_15/make versioning=loose
makedepends="$makedepends make=4.3-r0"

source="$pkgname-$pkgver.tar.gz::https://github.com/vatesfr/xen-orchestra/archive/$_commit.tar.gz
		enable_hub_community.patch
		gh_issue_redirect.patch
		js_yaml_not_installed.patch
		no_banner.patch
		no_menu_warnings.patch
		yarnclean"
pkgusers='root'
pkggroups='root'
options='!check'
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare
	rm -r packages/xo-server-test
	rm -r packages/xo-server-test-plugin
}

build() {
	yarn install --pure-lockfile --non-interactive
	yarn build
	cp "$srcdir/yarnclean" .yarnclean
	yarn install --pure-lockfile --non-interactive --production
}

package() {
	install -dm755 "$pkgdir/app/$pkgname"
	cp -r "$srcdir/$pkgname-$_commit/." "$pkgdir/app/$pkgname"
	cd "$pkgdir/app/$pkgname/packages/xo-server/node_modules/" && ln -s ../../xo-server-* ./ && cd -
	rm -r \
		"$pkgdir/app/$pkgname/node_modules/leveldown/prebuilds/android-arm" \
		"$pkgdir/app/$pkgname/node_modules/leveldown/prebuilds/android-arm64" \
		"$pkgdir/app/$pkgname/node_modules/leveldown/prebuilds/darwin-x64+arm64" \
		"$pkgdir/app/$pkgname/node_modules/leveldown/prebuilds/linux-arm" \
		"$pkgdir/app/$pkgname/node_modules/leveldown/prebuilds/linux-arm64" \
		"$pkgdir/app/$pkgname/node_modules/leveldown/prebuilds/win32-ia32" \
		"$pkgdir/app/$pkgname/node_modules/leveldown/prebuilds/win32-x64"

	install -dm755 "$pkgdir/usr/bin"
	ln -s "/app/$pkgname/node_modules/.bin/xo-server" "$pkgdir/usr/bin/"
	find "$pkgdir/app/$pkgname" -name '*.ts' -type f -delete
}
sha512sums="
19da1c1f1fbf58d51b5efffc5f9ca84ec8a95af4a46b6030118232fa4df561c5f2a54c9cbef6b8bf563291ec8342e51c3827cc3d0f7c975329c8d425762d98db  xen-orchestra-5.69.2.tar.gz
d23b7adfb2729a9a302a619738146b93628728a1d96f2dfec68272e177faf5954b9dfe01fd623780c5ebcc07236b64e537fa458475354d0025faccd29728f124  enable_hub_community.patch
821bc73ed43dacb0d7eec78a5428e1798bb365ef42d3c013d65225f0e5e01b9c422e0ac847c8e10481bf023dc51825c883435b313a84df686d88ea3ba60da85d  gh_issue_redirect.patch
cc6a8ebdcbeb4bddae856680bff350cab4216ad0711eebb043fa09499dfebe72039d04a3886fc7bab2a9b8b98afa07659cc1fbfeedc071a8486515f2bcc251e2  js_yaml_not_installed.patch
fcbe0c68b5d53c6157266b5db3f762640cc99fa129d946919240cf2a06cb3d8206a3398c5764cc506e0a4aef89e2378a983a5ce61904d36b6e877d5759bfe2bd  no_banner.patch
eb110fdcf31a09f30d622905b972a16b37f904e85f5dde967fd04c4dd55fc94dd97f3c6c6856a5ebd81d5e216f631690450188bd88e9e7101f6184cac7137b43  no_menu_warnings.patch
dcd068f64a679632f853d1008c7d37c2fb9a4abfd43853bcf354c2b11252366c493a32afb67ec28d3482ceaaabc1850203a2f18b02f6b9bdf7e1b904e3964704  yarnclean
"
